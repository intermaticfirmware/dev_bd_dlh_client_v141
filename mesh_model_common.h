//-----------------------------------------------------------------------------
//   All Rights Reserved
//  Intermatic Incorporated
//  Copyright 2019
//-----------------------------------------------------------------------------
//
//  Description: mesh_model_common.h contains constants, macros, structure
//  definitions, and public prototypes that are common to all models
//
//  Here is where you would put a function common to more than one models
//
//-----------------------------------------------------------------------------

#ifndef _MESH_MODEL_COMMON_H_
#define _MESH_MODEL_COMMON_H_

#include "bg_types.h"
#include "bg_errorcodes.h"
#include "main.h"

// #defines and typedefs
#define LIGHT_LC_SERVER_PRESENT 1
#define GENERIC_DEFAULT_TRANSITION_SERVER_PRESENT 1
#if (DEVICE_TYPE == DEVICE_TYPE_OCC_SERVER)
    #define USE_NCP 0  // Sensors do NOT utilize NCP
#elif (DEVICE_TYPE == DEVICE_TYPE_DLH_SERVER)
    #define USE_NCP 0  // Sensors do NOT utilize NCP
#else
    #define USE_NCP 1
#endif

#define MAX_TIMER_24_TIME 0x00ffffff

typedef enum
{
    SENSOR_GET = 0x01,
    SENSOR_SETTINGS_SET,
    SENSOR_STATUS,
    SENSOR_SETTING_STATUS,

    // these need to match what the host has in task_ncp.c
    SETTINGS_HOST_OP_GET,
    SETTINGS_HOST_OP_SET,
    SETTINGS_HOST_OP_SET_UNACK,
    SETTINGS_HOST_OP_SET_ALL,

    SETTINGS_MODULE_OP_GET,
    SETTINGS_MODULE_OP_SET,
    SETTINGS_MODULE_OP_SET_UNACK,

    LC_MODE_OP_GET,
    LC_MODE_OP_SET,
    LC_MODE_OP_SET_UNACK,
    LC_MODE_OP_STATUS,

    LC_OCCUPANCY_MODE_OP_GET,
    LC_OCCUPANCY_MODE_OP_SET,
    LC_OCCUPANCY_MODE_OP_SET_UNACK,
    LC_OCCUPANCY_MODE_OP_STATUS,

    LC_ONOFF_OP_GET,
    LC_ONOFF_OP_SET,
    LC_ONOFF_OP_SET_UNACK,
    LC_ONOFF_OP_STATUS,

    LC_PROPERTY_OP_GET,
    LC_PROPERTY_OP_SET,
    LC_PROPERTY_OP_SET_UNACK,
    LC_PROPERTY_OP_STATUS,

    SENSOR_SETTINGS_GET,
	SENSOR_CADENCE_GET,
	SENSOR_CADENCE_SET,
	SENSOR_CADENCE_SET_UNACK,
	SENSOR_CADENCE_STATUS,
	SENSOR_CUSTOM_SETTINGS_OP_GET,
	SENSOR_CUSTOM_SETTINGS_OP_SET,
	SENSOR_CUSTOM_SETTINGS_OP_SET_UNACK,

    // max op codes is 63. It is 6 bits
} SHARED_VENDOR_OP_CODES_E;

// global functions
extern uint8_t* get_api_error_text(errorcode_t error_code);

#endif // _MESH_MODEL_COMMON_H_
