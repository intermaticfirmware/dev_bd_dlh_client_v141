//-----------------------------------------------------------------------------
//   All Rights Reserved
//  Intermatic Incorporated
//  Copyright 2019
//-----------------------------------------------------------------------------
//
//  Description: hal-config-devbd.h contains constants, macros, structure definitions,
//               and public prototypes.
//
//-----------------------------------------------------------------------------
#ifndef HAL_CONFIG_DEVBD_H
#define HAL_CONFIG_DEVBD_H

#define DEV_BOARD 1 // Blue Gecko dev board


#if DEV_BOARD
// HOST MCU UART port
//
// On EFR32 (EFR32BG13P) Blue Gecko dev board: J101 P9 TX, P11 RX
//
#define HOST_MCU_UART_PORT								(gpioPortA)

#define HOST_MCU_TXD									(0U)
#define HOST_MCU_RXD									(1U)
#define HOST_RXLOC                                      LEUART_ROUTELOC0_RXLOC_LOC0
#define HOST_TXLOC                                      LEUART_ROUTELOC0_TXLOC_LOC0

#else
// HOST MCU UART port
//
// On ALC_DT_BT_C4_BT PRGRM_RX, PRGRM_TX on schematic
//
#define HOST_MCU_UART_PORT								(gpioPortA)

#define HOST_MCU_TXD									(0U)
#define HOST_MCU_RXD									(1U)
#define HOST_RXLOC                                      LEUART_ROUTELOC0_RXLOC_LOC0
#define HOST_TXLOC                                      LEUART_ROUTELOC0_TXLOC_LOC0

#endif

#if DEV_BOARD
// Debug UART port
//
// On EFR32 (EFR32BG13P) Blue Gecko dev board: J101 P5 TX, P7 RX
//
#define DBG_PORT										(gpioPortC)

#define DBG_RX_PIN                                  	(9U)
#define DBG_TX_PIN                                  	(8U)
#define DBG_RXLOC                                       USART_ROUTELOC0_RXLOC_LOC13
#define DBG_TXLOC                                       USART_ROUTELOC0_TXLOC_LOC13
#else
// Debug UART port
//
// On ALC_DT_BT_C4_BT CLI_RX, CLI_TX on schematic
//
#define DBG_PORT										(gpioPortC)

#define DBG_RX_PIN                                  	(11U)
#define DBG_TX_PIN                                  	(10U)
#define DBG_RXLOC                                       USART_ROUTELOC0_RXLOC_LOC15
#define DBG_TXLOC                                       USART_ROUTELOC0_TXLOC_LOC15

#endif

#if DEV_BOARD
// Blue Gecko dev board
#else
//
// On ALC_DT_BT_C4_B PRGRM_RESET, PRGRM_ACCESS
//
#define PRGRM_PORT                                      (gpioPortA)
#define PRGRM_RESET_PIN                                 (3U)
#define PRGRM_ACCESS_PIN                                (2U)

//
// On ALC_DT_BT_C4_B RED, BLUE LED
//
#define LED_PORT                                        (gpioPortD)
#define BLUE_LED_PIN                                    (13U)
#define RED_LED_PIN                                     (15U)

//
// RESET button on actual board
//
#define RESET_BUTTON_PORT                              (gpioPortD)
#define RESET_BUTTON_PIN                               (14U)
#endif

#endif /* HAL_CONFIG_DEVBD_H */
