# dev_bd_occ_client_v141

This is test software for testing the dev_bd_occ_server_v141 on an EmCom
board. Building using the DEV_BOARD set to 1 will allow this software to be used
on a Blue Gecko dev board.  When DEV_BOARD is set to 0 the build can be used on 
another EmCom board.

- flash software onto a dev kit (BRD4104A) or a EmCom board

- provision device using dev_bd_provisioner_v141 on dev kit (BRD4104A)

- or use self provisioning by setting SELF_PROVISION_ENABLED 1

- pressing PB0 while reseting board will do a factory reset
(push and hold PB0 then use reset then release PB0)

- PB0 long or short press publishes sensor get msg 

- PB1 long press publishes sensor_settings_set message (amb_light_settings)

- PB1 short press publishes sensor_settings_get message (trig_settings)

- provisioner sets up the publish / subscription addresses

DEBUG communications

On dev board J101: P5 TX, P7 RX

USART1 Port C pin 8 TX, Port C pin 9 RX 115200 BAUD NO PARITY, 1 STOP BIT

On EmCom board CLI port:

USART1 Port C pin 10 TX, Port C pin 11 RX 115200 BAUD NO PARITY, 1 STOP BIT


older version uses 

- use segger tool JLinkRTTViewer.exe (using BGM13P32F512GA) to see display output 
(EFR32BG13P)