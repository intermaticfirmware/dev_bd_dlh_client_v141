/*
 * timer_events.h
 *
 *  Created on: Mar 20, 2019
 *      Author: reverett
 */

#ifndef TIMER_EVENTS_H_
#define TIMER_EVENTS_H_

#define TIMER_ID_FACTORY_RESET  77
#define TIMER_ID_PERIODICAL_UPDATE						50

#define TIMER_ID_RESET 10

#define TIMER_ID_ACK   11
#define TIMER_ID_SYNC  12

#endif /* TIMER_EVENTS_H_ */
