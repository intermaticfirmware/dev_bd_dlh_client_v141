//-----------------------------------------------------------------------------
//  All Rights Reserved
//  Intermatic Incorporated
//  Copyright 2019
//-----------------------------------------------------------------------------
//
//  Description: Contains the initialization, state machine, and messaging
//                  for the model that contains custom settings
//-----------------------------------------------------------------------------

//    Include Files
//******************************************************************************
#include <stdio.h>
#include "mesh_custom_settings_vendor.h"
#include "mesh_model_common.h"
//#include "timer.h"
//#include "nvm.h"
#include "debug.h"
//#include "main.h"
//#include "wireless.h"

//    Constants & Definitions  (File-scope)
//******************************************************************************
#define SETTINGS_NUMBER_OF_OP_CODES     3


//    Structures, enums, etc.  (File-scope)
//******************************************************************************
typedef struct
{
    SETTINGS_NVM_T *nvm;
} SETTINGS_STATES_T;

typedef struct
{
    uint16 elem_index;
    uint16 vendor_id;
    uint16 model_id;
    uint8 publish; // publish - 1, not - 0
    uint8 opcodes_len;
    uint8 opcodes_data[SETTINGS_NUMBER_OF_OP_CODES];
} SETTINGS_VENDOR_DATA_T;

typedef struct
{
    SHARED_VENDOR_OP_CODES_E opcode;
    uint8_t requires_ack;
    uint8_t requires_status_update;
} SETTINGS_VENDOR_OP_CODE_T;


//    Variable declarations  (File-scope / static)
//******************************************************************************
//static SETTINGS_STATES_T settings_states;

static SETTINGS_VENDOR_DATA_T settings_model =
{
    .elem_index = SETTINGS_VENDOR_ELEMENT,
    .vendor_id = SETTINGS_VENDOR_ID,
    .model_id = SETTINGS_VENDOR_MODEL_ID,
    .publish = 1,
    .opcodes_len = SETTINGS_NUMBER_OF_OP_CODES
};

static const SETTINGS_VENDOR_OP_CODE_T op_codes[SETTINGS_NUMBER_OF_OP_CODES] =
{
    {SENSOR_CUSTOM_SETTINGS_OP_GET, 0, 0},   // while GET does respond it does not respond directly here because we must wait for data from the host
    {SENSOR_CUSTOM_SETTINGS_OP_SET, 1, 0},
    {SENSOR_CUSTOM_SETTINGS_OP_SET_UNACK, 0, 0},

    // Add here and in SHARED_VENDOR_OP_CODES_E
};

struct gecko_msg_mesh_vendor_model_receive_evt_t recv_evt;


//    Function prototypes - private members
//******************************************************************************


//--------------------------------------------------------------------------------
// NAME      : settings_model_init
// ABSTRACT  : This initializes the Settings vendor model
// ARGUMENTS : None
// RETURN    :
//  errorcode_t       : result of gecko_cmd_mesh_vendor_model_init()
//
// Note      : Must call gecko_bgapi_class_mesh_vendor_model_init() for running this
//              or any other functions here.
//--------------------------------------------------------------------------------
errorcode_t settings_model_init(void)
{
    for (int i = 0; i < SETTINGS_NUMBER_OF_OP_CODES; i++)
    {
        settings_model.opcodes_data[i] = op_codes[i].opcode;  // must populate this before calling gecko_cmd_mesh_vendor_model_init()
    }

    errorcode_t retval = gecko_cmd_mesh_vendor_model_init(settings_model.elem_index, settings_model.vendor_id, settings_model.model_id, settings_model.publish, settings_model.opcodes_len, settings_model.opcodes_data)->result;
    DEBUGV(LOGI, "Sensor Custom Settings model init error = %s\n\r", get_api_error_text(retval));

    return retval;
}

//--------------------------------------------------------------------------------
// NAME      : settings_publish
// ABSTRACT  : This publish data from the settings model.
// ARGUMENTS :
//  vendor_opcodes_t opcode - op code to send with
//  uint8_t length - Data length to send
//  uint8_t * data - Data buffer to send
// RETURN    :
//   errorcode_t       : result of gecko_cmd_mesh_vendor calls
//--------------------------------------------------------------------------------
errorcode_t settings_publish(SHARED_VENDOR_OP_CODES_E opcode, uint8_t length, uint8_t * data)
{
    errorcode_t retval = gecko_cmd_mesh_vendor_model_set_publication(settings_model.elem_index, settings_model.vendor_id, settings_model.model_id, opcode, 1, length, data)->result;

    if (retval)
    {
        DEBUGV(LOGE, "Set publication error = %s\r\n", get_api_error_text(retval));
    }
    else
    {
        retval = gecko_cmd_mesh_vendor_model_publish(settings_model.elem_index, settings_model.vendor_id, settings_model.model_id)->result;

        if (retval)
        {
            DEBUGV(LOGE, "Publish error = %s\r\n", get_api_error_text(retval));
        }
    }

    return retval;
}

//--------------------------------------------------------------------------------
// NAME      : gecko_evt_mesh_settings_vendor_model_receive_id_action
// ABSTRACT  : This handles receiving data from the settings vendor client.
//              place this under the gecko_evt_mesh_vendor_model_receive_id case
//              in handle_gecko_event()
// ARGUMENTS :
//  struct gecko_cmd_packet *evt - event passed from handle_gecko_event()
// RETURN    : None
//--------------------------------------------------------------------------------
uint8_t gecko_evt_mesh_settings_vendor_model_receive_id_action(struct gecko_cmd_packet *evt)
{
    memcpy(&recv_evt, &evt->data, sizeof(recv_evt)); // warning this does not copy the contents of evt->data.evt_mesh_vendor_model_receive.payload.data. Would need to malloc some space for the local recv_evt.payload.data
    SETTINGS_MESSAGE_DATA_T *received_data = (SETTINGS_MESSAGE_DATA_T *)&evt->data.evt_mesh_vendor_model_receive.payload.data;

    DEBUGV(LOGI, "Sensor Custom Settings model data received.\r\n");
    DEBUGV(LOGI, "    Element index = %d\r\n", recv_evt.elem_index);
    DEBUGV(LOGI, "    Vendor id = 0x%04X\r\n", recv_evt.vendor_id);
    DEBUGV(LOGI, "    Model id = 0x%04X\r\n", recv_evt.model_id);
    DEBUGV(LOGI, "    Source address = 0x%04X\r\n", recv_evt.source_address);
    DEBUGV(LOGI, "    Destination address = 0x%04X\r\n", recv_evt.destination_address);
    DEBUGV(LOGI, "    Destination label UUID index = 0x%02X\r\n", recv_evt.va_index);
    DEBUGV(LOGI, "    App key index = 0x%04X\r\n", recv_evt.appkey_index);
    DEBUGV(LOGI, "    Non-relayed = 0x%02X\r\n", recv_evt.nonrelayed);
    DEBUGV(LOGI, "    Final = 0x%04X\r\n", recv_evt.final);
    DEBUGV(LOGI, "    Op Code = 0x%02X\r\n", recv_evt.opcode);
    DEBUGV(LOGI, "    Property ID = 0x%04X\r\n", received_data->header.property_id);

    DEBUGV(LOGI, "\tPayload, length %d:", evt->data.evt_mesh_vendor_model_receive.payload.len);
    for (uint8_t i = 0; i < evt->data.evt_mesh_vendor_model_receive.payload.len; i++)
    {
        DEBUGV(LOGI, " 0x%02X", evt->data.evt_mesh_vendor_model_receive.payload.data[i]);
    }
    DEBUGV(LOGI, "\n\r");

    switch (recv_evt.opcode)
    {
        case SENSOR_CUSTOM_SETTINGS_OP_SET:
        case SENSOR_CUSTOM_SETTINGS_OP_SET_UNACK:
            break;

        case SENSOR_CUSTOM_SETTINGS_OP_GET:
            switch (received_data->header.property_id)
            {
                case SETTINGS_WINK:
                    DEBUGV(LOGD, "    Payload: SETTINGS_WINK  setting = 0x%02X\r\n", received_data->wink_time_secs);
                    break;
                case SETTINGS_GET_SENSOR_MODEL:
                    DEBUGV(LOGD, "    Payload: SETTINGS_GET_SENSOR_MODEL  setting = 0x%02X\r\n", received_data->host_model_num);
                    break;
                case SETTINGS_GET_POWER_CFG:
                    DEBUGV(LOGD, "    Payload: SETTINGS_GET_POWER_CFG  setting = 0x%02X\r\n", received_data->power_type);
                    break;
                case SETTINGS_GET_HOST_FW_VERSION:
                    DEBUGV(LOGD, "    Payload: SETTINGS_GET_HOST_FW_VERSION  setting = %s\r\n", received_data->string);
                    break;
                case SETTINGS_GET_SCM_FW_VERSION_GET:
                    DEBUGV(LOGD, "    Payload: SETTINGS_GET_SCM_FW_VERSION_GET  setting = %s\r\n", received_data->string);
                    break;
                case SETTINGS_SET_ONOFF_SWITCH_CFG:
                    DEBUGV(LOGD, "    Payload: SETTINGS_SET_ONOFF_SWITCH_CFG  setting = 0x%02X\r\n", received_data->onoff_switch_cfg);
                    break;
                case SETTINGS_SET_WIRED_LIGHT_OUTPUT:
                    DEBUGV(LOGD, "    Payload: SETTINGS_SET_WIRED_LIGHT_OUTPUT  setting = 0x%02X\r\n", received_data->wired_light_output);
                    break;
                case SETTINGS_GET_LOWBATTERY:
                    DEBUGV(LOGD, "    Payload: SETTINGS_GET_LOWBATTERY  setting = 0x%02X\r\n", received_data->host_lowbatt);
                    break;
                case SETTINGS_GET_HOST_ERROR:
                    DEBUGV(LOGD, "    Payload: SETTINGS_GET_HOST_ERROR  setting = 0x%02X\r\n", received_data->host_error);
                    break;
                default:
                    DEBUGV(LOGD, "    Payload:  INVALID Property ID\r\n");
                    break;
            }
            break;

        default:
            // Client should not response to other opcodes
            DEBUGV(LOGD, "Error:  Invalid Opcode\r\n");
            break;
    }

    return 0;
}
